Denkmal API
===========

Configuration
-------------

The application is configured with environment variables.

General:
- `ADMIN_URL`: Base URL of the admin frontend
- `ENCRYPTION_KEY`: Secret encryption key

Logging:
- `ELASTICSEARCH_URL`: Elasticsearch URL for logging (optional)
- `LOG_LEVEL`: Log level for console ("debug", "info", "warn" or "error)

Database:
- `RDS_POSTGRES_URL` Full postgres URL
- `DATABASE_HOST`: Postgres hostname
- `DATABASE_HOST_TEST`: Postgres hostname for running tests

Email:
- `SES_ACCESS_KEY_ID`: AWS SES key
- `SES_SECRET_ACCESS_KEY`: AWS SES secret
- `SES_SENDER_EMAIL`: Sender email address


Development
-----------

### Setup

Install dependencies.
```
yarn install
```

### Run locally with NodeJS

Start a postgres database:
```
docker run -e TZ=UTC -e PGTZ=UTC -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres -p 5432:5432 postgres:10-alpine
```

Start the application:
```
yarn start
```

The service now runs on port 5000 and nodemon watches for changes in the src directory.
- API endpoint: http://localhost:5000/graphql
- GraphQL sandbox: http://localhost:5000/graphql (using a browser)


### Alternatively, run locally with docker-compose

Build image:
```
docker-compose build
```

Start database and application:
```
docker-compose up
```


### Run tests

A separate test db needs to run under port 5433
```
docker run -e TZ=UTC -e PGTZ=UTC -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres -p 5433:5432 postgres:10-alpine
```

Once the database is ready run the tests
```
yarn test
```

### Database migrations
All database migrations are run automatically on application start.
If the schema has been changed, a new migration has to be created:
```
yarn typeorm migration:generate -n {migration name} src/migration
```

### Create new entity
```
yarn typeorm entity:create -n {entity name}
```
After creating a new entity, the entity needs to be referenced in `ormconfig.yml` for it to be discovered by the database migration generation.
Also, the entity must be added to the database ConnectionOptions inside `database.ts`.

### Load Mock Data
To load mock data into postgres (it will first flush the DB) run the "addMockData" script:
```
yarn run addMockData
```

### Profiling with 0x

1: Install [0x](https://github.com/davidmarkclements/0x):
```
sudo yarn global add 0x
```

2: Run "serve" with 0x:
```
0x -o node_modules/.bin/ts-node -r tsconfig-paths/register ./src/server.ts
```

3: Generate some load on the application.

4: Press Ctrl+C to stop the server. A flamegraph chart will open automatically in your browser.


Releasing
---------
1. Push new code to the master branch
2. Gitlab CI job "deploy" will build a docker image and release it to Heroku
