import {SESClient, SendEmailCommand} from "@aws-sdk/client-ses";
import {AwsCredentialIdentity} from "@aws-sdk/types";
import {SendEmailCommandInput} from "@aws-sdk/client-ses/dist-types/commands/SendEmailCommand";

const globalContext: any = global;


export const sendMail = (message: string, subject: string, receivers: string[]) => {
    const credentials: AwsCredentialIdentity = {
        accessKeyId: process.env.SES_ACCESS_KEY_ID,
        secretAccessKey: process.env.SES_SECRET_ACCESS_KEY
    };

    const sesClient = new SESClient({
        region: 'eu-west-1',
        credentials: credentials,
    });

    const params: SendEmailCommandInput = {
        Destination: {
            BccAddresses: receivers
        },
        Message: {
            Body: {
                Text: {
                    Charset: "UTF-8",
                    Data: message
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        },
        Source: process.env.SES_SENDER_EMAIL || "notifications@denkmal.org"
    };

    const command = new SendEmailCommand(params);

    sesClient.send(command).then(
        function (data) {
            globalContext.logger.debug(`Mail sent ${data.MessageId}`);
        }).catch(
        function (err) {
            globalContext.logger.error(`Mail failed to send: ${err}`);
        }
    );
};