import {MigrationInterface, QueryRunner} from "typeorm";

export class tags1568549883304 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "eventVersions" ADD "tags" text array`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "eventVersions" DROP COLUMN "tags"`);
    }

}
