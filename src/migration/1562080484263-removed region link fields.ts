import {MigrationInterface, QueryRunner} from "typeorm";

export class removedRegionLinkFields1562080484263 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "regions" DROP COLUMN "facebookPage"`);
        await queryRunner.query(`ALTER TABLE "regions" DROP COLUMN "twitterAccount"`);
        await queryRunner.query(`ALTER TABLE "regions" DROP COLUMN "footerUrl"`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "regions" ADD "footerUrl" text`);
        await queryRunner.query(`ALTER TABLE "regions" ADD "twitterAccount" text`);
        await queryRunner.query(`ALTER TABLE "regions" ADD "facebookPage" text`);
    }

}
