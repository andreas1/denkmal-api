import {MigrationInterface, QueryRunner} from "typeorm";

export class init1553787265325 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" text NOT NULL, "email" text NOT NULL, "password" text NOT NULL, "accessLevel" text NOT NULL DEFAULT 'Regional', "loginKey" text NOT NULL, "regionId" uuid, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "eventVersions" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "from" TIMESTAMP NOT NULL, "until" TIMESTAMP, "description" text, "isReviewPending" boolean NOT NULL DEFAULT true, "hasTime" boolean NOT NULL DEFAULT true, "links" text NOT NULL DEFAULT '[]', "sourceType" text NOT NULL, "sourceIdentifier" text, "sourceUrl" text, "sourcePriority" integer, "eventId" uuid NOT NULL, CONSTRAINT "PK_a1c2c901becd9cc54594f463682" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_1835627c29bee42025fd4c8da1" ON "eventVersions" ("from") `);
        await queryRunner.query(`CREATE INDEX "IDX_3b17facb96563e6bd3ac8988a0" ON "eventVersions" ("until") `);
        await queryRunner.query(`CREATE INDEX "IDX_a6f6eaf17bcf2e5a2a8894ca5f" ON "eventVersions" ("eventId") `);
        await queryRunner.query(`CREATE TABLE "genres" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "isReviewPending" boolean NOT NULL DEFAULT true, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "categoryId" uuid, CONSTRAINT "UQ_228ad8d2c89a3060f7d6749aa24" UNIQUE ("categoryId", "name"), CONSTRAINT "PK_80ecd718f0f00dde5d77a9be842" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_f105f8230a83b86a346427de94" ON "genres" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_07617f66793b27b89ecffb7b40" ON "genres" ("categoryId") `);
        await queryRunner.query(`CREATE TABLE "genreCategories" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "color" character varying, "regionId" uuid NOT NULL, CONSTRAINT "UQ_81700db8b8960eaf29a5e2abe5f" UNIQUE ("regionId", "name"), CONSTRAINT "PK_25741c401fa2f50187cf8f5920a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d596ebba07331cc06ad733b3bc" ON "genreCategories" ("name") `);
        await queryRunner.query(`CREATE INDEX "IDX_dff9d33c6aa0ac858a692ab6d0" ON "genreCategories" ("regionId") `);
        await queryRunner.query(`CREATE TABLE "regions" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" text NOT NULL, "email" text NOT NULL, "slug" text NOT NULL, "latitude" real NOT NULL, "longitude" real NOT NULL, "timeZone" text NOT NULL DEFAULT 'UTC', "dayOffset" real NOT NULL DEFAULT 5, "suspendedUntil" TIMESTAMP, "facebookPage" text, "twitterAccount" text, "footerUrl" text, CONSTRAINT "UQ_53cf784f23cbf14bb7717e969d4" UNIQUE ("slug"), CONSTRAINT "PK_4fcd12ed6a046276e2deb08801c" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_53cf784f23cbf14bb7717e969d" ON "regions" ("slug") `);
        await queryRunner.query(`CREATE TABLE "venueAliases" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" text NOT NULL, "regionId" uuid NOT NULL, "venueId" uuid NOT NULL, CONSTRAINT "UQ_281812618135c2a090d4ac5b79f" UNIQUE ("regionId", "name"), CONSTRAINT "PK_6987ba6da4e6c708381575cf644" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_fc256d79b45cda558d4024db5c" ON "venueAliases" ("venueId") `);
        await queryRunner.query(`CREATE TABLE "venues" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" text NOT NULL, "address" text, "url" text, "email" text, "latitude" real, "longitude" real, "isReviewPending" boolean NOT NULL DEFAULT false, "isSuspended" boolean NOT NULL DEFAULT false, "ignoreScraper" boolean NOT NULL DEFAULT false, "facebookPageId" text, "twitter" text, "regionId" uuid NOT NULL, CONSTRAINT "UQ_73572198ed3eeb3c6d345fcaa1b" UNIQUE ("regionId", "name"), CONSTRAINT "PK_cb0f885278d12384eb7a81818be" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "events" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "isHidden" boolean NOT NULL DEFAULT false, "isPromoted" boolean NOT NULL DEFAULT false, "regionId" uuid NOT NULL, "activeVersionId" uuid, "venueId" uuid NOT NULL, CONSTRAINT "REL_4d8bec856c1b140994d0b84784" UNIQUE ("activeVersionId"), CONSTRAINT "PK_40731c7151fe4be3116e45ddf73" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_4d8bec856c1b140994d0b84784" ON "events" ("activeVersionId") `);
        await queryRunner.query(`CREATE INDEX "IDX_0af7bb0535bc01f3c130cfe5fe" ON "events" ("venueId") `);
        await queryRunner.query(`CREATE TABLE "event_versions_genres_genres" ("eventVersionsId" uuid NOT NULL, "genresId" uuid NOT NULL, CONSTRAINT "PK_5f6a6f274a1885d2b78697ffa15" PRIMARY KEY ("eventVersionsId", "genresId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_a25f02326d62b4ef30563e0a62" ON "event_versions_genres_genres" ("eventVersionsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_28c1e9a5cd2539e514abf8c02b" ON "event_versions_genres_genres" ("genresId") `);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_2c69c851bdaed5b02825d6f6d50" FOREIGN KEY ("regionId") REFERENCES "regions"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "eventVersions" ADD CONSTRAINT "FK_a6f6eaf17bcf2e5a2a8894ca5f2" FOREIGN KEY ("eventId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "genres" ADD CONSTRAINT "FK_07617f66793b27b89ecffb7b409" FOREIGN KEY ("categoryId") REFERENCES "genreCategories"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "genreCategories" ADD CONSTRAINT "FK_dff9d33c6aa0ac858a692ab6d0c" FOREIGN KEY ("regionId") REFERENCES "regions"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "venueAliases" ADD CONSTRAINT "FK_fc256d79b45cda558d4024db5ca" FOREIGN KEY ("venueId") REFERENCES "venues"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "venues" ADD CONSTRAINT "FK_c0bea08d1b69d44bcdf18c9759b" FOREIGN KEY ("regionId") REFERENCES "regions"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "events" ADD CONSTRAINT "FK_4d8bec856c1b140994d0b847845" FOREIGN KEY ("activeVersionId") REFERENCES "eventVersions"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "events" ADD CONSTRAINT "FK_0af7bb0535bc01f3c130cfe5fe7" FOREIGN KEY ("venueId") REFERENCES "venues"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "event_versions_genres_genres" ADD CONSTRAINT "FK_a25f02326d62b4ef30563e0a628" FOREIGN KEY ("eventVersionsId") REFERENCES "eventVersions"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "event_versions_genres_genres" ADD CONSTRAINT "FK_28c1e9a5cd2539e514abf8c02b7" FOREIGN KEY ("genresId") REFERENCES "genres"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "event_versions_genres_genres" DROP CONSTRAINT "FK_28c1e9a5cd2539e514abf8c02b7"`);
        await queryRunner.query(`ALTER TABLE "event_versions_genres_genres" DROP CONSTRAINT "FK_a25f02326d62b4ef30563e0a628"`);
        await queryRunner.query(`ALTER TABLE "events" DROP CONSTRAINT "FK_0af7bb0535bc01f3c130cfe5fe7"`);
        await queryRunner.query(`ALTER TABLE "events" DROP CONSTRAINT "FK_4d8bec856c1b140994d0b847845"`);
        await queryRunner.query(`ALTER TABLE "venues" DROP CONSTRAINT "FK_c0bea08d1b69d44bcdf18c9759b"`);
        await queryRunner.query(`ALTER TABLE "venueAliases" DROP CONSTRAINT "FK_fc256d79b45cda558d4024db5ca"`);
        await queryRunner.query(`ALTER TABLE "genreCategories" DROP CONSTRAINT "FK_dff9d33c6aa0ac858a692ab6d0c"`);
        await queryRunner.query(`ALTER TABLE "genres" DROP CONSTRAINT "FK_07617f66793b27b89ecffb7b409"`);
        await queryRunner.query(`ALTER TABLE "eventVersions" DROP CONSTRAINT "FK_a6f6eaf17bcf2e5a2a8894ca5f2"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_2c69c851bdaed5b02825d6f6d50"`);
        await queryRunner.query(`DROP INDEX "IDX_28c1e9a5cd2539e514abf8c02b"`);
        await queryRunner.query(`DROP INDEX "IDX_a25f02326d62b4ef30563e0a62"`);
        await queryRunner.query(`DROP TABLE "event_versions_genres_genres"`);
        await queryRunner.query(`DROP INDEX "IDX_0af7bb0535bc01f3c130cfe5fe"`);
        await queryRunner.query(`DROP INDEX "IDX_4d8bec856c1b140994d0b84784"`);
        await queryRunner.query(`DROP TABLE "events"`);
        await queryRunner.query(`DROP TABLE "venues"`);
        await queryRunner.query(`DROP INDEX "IDX_fc256d79b45cda558d4024db5c"`);
        await queryRunner.query(`DROP TABLE "venueAliases"`);
        await queryRunner.query(`DROP INDEX "IDX_53cf784f23cbf14bb7717e969d"`);
        await queryRunner.query(`DROP TABLE "regions"`);
        await queryRunner.query(`DROP INDEX "IDX_dff9d33c6aa0ac858a692ab6d0"`);
        await queryRunner.query(`DROP INDEX "IDX_d596ebba07331cc06ad733b3bc"`);
        await queryRunner.query(`DROP TABLE "genreCategories"`);
        await queryRunner.query(`DROP INDEX "IDX_07617f66793b27b89ecffb7b40"`);
        await queryRunner.query(`DROP INDEX "IDX_f105f8230a83b86a346427de94"`);
        await queryRunner.query(`DROP TABLE "genres"`);
        await queryRunner.query(`DROP INDEX "IDX_a6f6eaf17bcf2e5a2a8894ca5f"`);
        await queryRunner.query(`DROP INDEX "IDX_3b17facb96563e6bd3ac8988a0"`);
        await queryRunner.query(`DROP INDEX "IDX_1835627c29bee42025fd4c8da1"`);
        await queryRunner.query(`DROP TABLE "eventVersions"`);
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
