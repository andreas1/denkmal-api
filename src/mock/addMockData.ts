import {EntityManager} from "typeorm";
import {Region} from "../entities/region";
import {Venue} from "../entities/venue";
import {EventVersion, SourceTypeEnum} from "../entities/eventVersion";
import {Event} from "../entities/event";
import {Genre} from "../entities/genre";
import {GenreCategory, unknownGenreCategoryName} from "../entities/genreCategory";
import {Level, WinstonLogger} from "../logger/logger";
import {initializeDataSource, truncateAllTables} from "../database/database";
import {AccessLevelEnum, User} from "../entities/user";
import {hashPassword} from "../api/authentication/hashPassword";
import casual = require("casual");
import moment = require("moment");


const addMockData = async function() {
    const logger = new WinstonLogger(
        {
            console: {level: Level.info},
        },{
            program: 'denkmal-api'
        });
    const dataSource = await initializeDataSource(false, logger);

    await truncateAllTables();

    await dataSource.manager.transaction(async function(transactionalEntityManager : EntityManager) {
        await addMockRegions(transactionalEntityManager);
        await addMockVenues(transactionalEntityManager);
        await addMockEvents(transactionalEntityManager);
        await addMockGenres(transactionalEntityManager);
        await addMockUsers(transactionalEntityManager);
    });
}

const addMockRegions = async function(entityManager : EntityManager) {
    const regionRepository = entityManager.getRepository(Region);
    {
        const r = new Region();
        r.id = "d34cc52d-7f88-4730-8917-f3a4b5d4f62b";
        r.name = "Basel";
        r.slug = "basel";
        r.latitude = 47.55457;
        r.longitude = 7.59032;
        r.email = "basel@denkmal.org";
        r.timeZone = "Europe/Zurich";

        await
            regionRepository.save(r);
    }
    {
        const r = new Region();
        r.id = "8649b2e1-9f11-4357-afe8-ba6fd775e756";
        r.name = "Zürich";
        r.slug = "tsri";
        r.latitude = 47.37723;
        r.longitude = 8.52735;
        r.email = "tsri@denkmal.org";
        r.timeZone = "Europe/Zurich";

        await
            regionRepository.save(r);
    }
}

const addMockVenues = async function(entityManager : EntityManager) {
    const regionRepository = entityManager.getRepository(Region);
    const venueRepository = entityManager.getRepository(Venue);

    const regions = await regionRepository.find();

    for (let i=0; i<regions.length; i++) {
        const r = regions[i];
        const venuesToGenerate = casual.integer(6, 16);

        for (let i = 1; i <= venuesToGenerate; i++) {
            const v = new Venue();
            v.name = casual.company_name;
            v.address = casual.address;
            v.email = casual.email;
            v.latitude = parseFloat(casual.latitude);
            v.longitude = parseFloat(casual.longitude);
            v.region = Promise.resolve(r);
            v.url = casual.url;

           await venueRepository.save(v);
        }
    }
}

const addMockEvents = async function(entityManager : EntityManager) {
    const eventRepository = entityManager.getRepository(Event);
    const regionRepository = entityManager.getRepository(Region);
    const eventVersionRepository = entityManager.getRepository(EventVersion);



    const startDate = moment().subtract(5, 'days');
    const endDate = startDate.clone().add(30, 'days');
    let days : moment.Moment[] = [];

    let currDate = startDate;
    while(endDate.diff(currDate) > 0) {
        days.push(currDate);
        currDate = moment(currDate).add(1, 'day');
    }


    const regions = await regionRepository.find();


    for (let regionI = 0; regionI < regions.length; regionI++) {
        const region = regions[regionI];
        const venues = await region.venues;

        for (let venueI = 0; venueI < venues.length; venueI++) {
            const v = venues[venueI];
            const eventsPerDayProbability = Math.random();

            for(let dayI = 0; dayI<days.length; dayI++) {
                const d = days[dayI];
                const hasEvent = eventsPerDayProbability > Math.random();

                if (hasEvent) {
                    const rawEvent = new Event();
                    rawEvent.venue = Promise.resolve(v);
                    rawEvent.isHidden = Math.random() > 0.95;
                    rawEvent.regionId = region.id;
                    const event = await eventRepository.save(rawEvent);

                    const date = moment(d).add(Math.random()*24, 'hour');
                    const rawMainVersion = new EventVersion();
                    rawMainVersion.description = casual.short_description;
                    rawMainVersion.from = date;
                    rawMainVersion.event = Promise.resolve(event);
                    if (Math.random() > 0.5) { rawMainVersion.links = [{label: casual.catch_phrase, url: casual.url}] };
                    rawMainVersion.isReviewPending = Math.random() > 0.95;
                    rawMainVersion.sourceType = SourceTypeEnum.Scraper;
                    rawMainVersion.sourceUrl = Math.random() > 0.5 ? casual.url : null;
                    rawMainVersion.sourceIdentifier = Math.random() > 0.5 ?"mock-data" : null;
                    if (Math.random() > 0.75) {
                        rawMainVersion.until = moment(date).add((Math.random())*120, 'minute')
                    }
                    const mainVersion = await eventVersionRepository.save(rawMainVersion);

                    event.activeVersion = mainVersion;
                    await eventRepository.save(event);

                    let additionalVersions = casual.integer(0,Math.random() > 0.95 ? 15: 3);

                    for (let i=0; i<additionalVersions; i++) {
                        const version = new EventVersion();
                        version.description = Math.random() > 0.8 ? casual.short_description : rawMainVersion.description,
                        version.from = Math.random() > 0.5 ? moment(rawMainVersion.from).add(casual.integer(1, 120), 'minutes') : rawMainVersion.from,
                        version.event = Promise.resolve(event);
                        version.isReviewPending = Math.random() > 0.95;
                        version.until = rawMainVersion.until;
                        version.event = Promise.resolve(event);
                        version.sourceType = rawMainVersion.sourceType;
                        version.sourceIdentifier = rawMainVersion.sourceIdentifier;
                        if (version.until && version.until.isSameOrBefore(version.from)) {
                            version.until = moment(version.from).add((Math.random())*120, 'minutes');
                        }
                        version.links = rawMainVersion.links;

                        await eventVersionRepository.save(version);
                    }
                }
            }
        }
    }
}

const addMockGenres = async function(entityManager : EntityManager) {
    const regionRepository = entityManager.getRepository(Region);
    const regions = await regionRepository.find();

    for (let regionI = 0; regionI < regions.length; regionI++) {
        const region = regions[regionI];

        const unknownCategory = new GenreCategory();
        unknownCategory.region = Promise.resolve(region);
        unknownCategory.name = unknownGenreCategoryName;
        unknownCategory.color = "FFFFFF";
        await entityManager.getRepository(GenreCategory).save(unknownCategory);

        const popCategory = new GenreCategory();
        popCategory.region = Promise.resolve(region);
        popCategory.name = "pop";
        popCategory.color = "EFCDEE";
        await entityManager.getRepository(GenreCategory).save(popCategory);

        const popGenres = [
            "pop", "disco", "funk"
        ];
        for (let i = 0; i<popGenres.length; i++) {
            const genre = new Genre();
            genre.name = popGenres[i];
            genre.category = popCategory;
            await entityManager.getRepository(Genre).save(genre);
        }

        const rockCategory = new GenreCategory();
        rockCategory.region = Promise.resolve(region);
        rockCategory.name = "rock";
        rockCategory.color = "DADADA";
        await entityManager.getRepository(GenreCategory).save(rockCategory);

        const rockGenres = [
            "rock", "hard rock", "metal", "indie", "punk"
        ];
        for (let i = 0; i<rockGenres.length; i++) {
            const genre = new Genre();
            genre.name = rockGenres[i];
            genre.category = rockCategory;
            genre.isReviewPending = false;
            await entityManager.getRepository(Genre).save(genre);
        }
    }
}

const addMockUsers = async function(entityManager : EntityManager) {
    const userRepository = entityManager.getRepository(User);

    const admin = new User();
    admin.name = "Admin user";
    admin.email = "admin@mock.com";
    admin.accessLevel = AccessLevelEnum.Admin;
    admin.password = hashPassword("asdf");
    await userRepository.save(admin);

    const regional = new User();
    regional.name = "Regional manager";
    regional.email = "region@mock.com";
    regional.accessLevel = AccessLevelEnum.Regional;
    regional.region = Promise.resolve((await entityManager.getRepository(Region).find())[0]);
    regional.password = hashPassword("asdf");
    await userRepository.save(regional);
}

addMockData().then(() => {
    console.log("done")
    process.exit()
});