import {getDataSource, initializeDataSource} from "../database/database";
import {gql} from "graphql-tag";
import {Region} from "../entities/region";
import {Venue} from "../entities/venue";
import {Event} from "../entities/event";
import {EventVersion, SourceTypeEnum} from "../entities/eventVersion";
import {createTestServer} from "../test/testServer";
import {AccessLevelEnum} from "../entities/user";
import moment = require("moment-timezone");

describe("resolvers test", () => {
    let apolloServer;

    beforeAll(async () => {
        apolloServer = await createTestServer(AccessLevelEnum.Admin);
    })

    beforeEach(async () => {
        const dataSource = await getDataSource();

        const regionRepository = dataSource.getRepository(Region);
        const venueRepository = dataSource.getRepository(Venue);
        const eventRepository = dataSource.getRepository(Event);
        const eventVersionRepository = dataSource.getRepository(EventVersion);

        for (let i = 1; i <= 2; i++) {
            const r = new Region();
            r.name = "basel." + i;
            r.slug = r.name;
            r.latitude = 2.2;
            r.longitude = 3.1;
            r.email = "jo@mail.com";
            r.timeZone = "Europe/Zurich";
            r.dayOffset = 5;

            await
                regionRepository.save(r);

            for (let i = 1; i <= 2; i++) {
                const v = new Venue();
                v.region = Promise.resolve(r);
                v.name = r.slug + "-venue-" + i;

                await venueRepository.save(v);

                for(let j = 1; j <= 2; j++) {
                    const rawEvent = new Event();
                    rawEvent.venue = Promise.resolve(v);
                    rawEvent.isHidden = false;
                    rawEvent.regionId = r.id;
                    const event = await eventRepository.save(rawEvent);

                    const version = new EventVersion();
                    version.description = v.name + "-event-" + j;
                    version.from = moment("2040-12-04T19:30:00.000Z").add(j, 'minutes').add(i, 'seconds');
                    version.sourceType = SourceTypeEnum.Admin;
                    version.event = Promise.resolve(event);
                    version.isReviewPending = false;
                    await eventVersionRepository.save(version);

                    event.activeVersion = version;
                    await eventRepository.save(event);
                }
            }
        }
    });

    afterEach(async () => {

    });

    test("Test hierarchies", async () => {
        const regionsQuery = await apolloServer.executeOperation({
            query: gql`
                query {
                    regions{
                        name
                        venues {name}
                        events(eventDays:["2040-12-04"]){
                            description
                            from
                            until
                            description
                            venue{
                                region {
                                    name
                                }
                            }
                        }
                    }
                }
            `
        });
        expect(regionsQuery).toMatchSnapshot("regions");
    })
});