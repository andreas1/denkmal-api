import {getDataSource} from "../../../database/database";
import {Region} from "../../../entities/region";

export const regionResolver = async function (obj, {id, slug}, context, info) {
    if (!id && !slug) {
        throw new Error("id or slug parameter required");
    }
    let options = {};
    if (slug) {
        options['where'] = {slug: slug};
    } else {
        options['where'] = {id: id};
    }

    const regionRepository = getDataSource().getRepository(Region);
    return regionRepository.findOneOrFail(options);
};