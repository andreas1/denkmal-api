import {GraphQLScalarType, Kind} from "graphql";
import {URL} from "url";

const isValidUrl = (string) => {
    try {
        new URL(string);
        return true;
    } catch (_) {
        return false;
    }
}

export const urlResolver = new GraphQLScalarType({
    name: 'URL',
    description: 'URL string',
    serialize(value) {
        return value;
    },
    parseValue(value) {
        if (isValidUrl(value)) {
            return value;
        }
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                if (isValidUrl(ast.value)) {
                    return ast.value;
                }
        }
    }
});