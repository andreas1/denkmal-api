import {Genre} from "../../../entities/genre";
import {getDataSource} from "../../../database/database";

export const genreResolver = async function (obj, {id}, context, info) {
    const repository = getDataSource().getRepository(Genre);

    return repository.findOneOrFail({where: {id: id}, relations: ["category"]});
};
