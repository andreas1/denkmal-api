import {Genre} from "../../../entities/genre";
import {getDataSource} from "../../../database/database";
import {In} from "typeorm";

export const genresResolver = async function (obj, {ids}, context, info) {
    const repository = getDataSource().getRepository(Genre);

    return repository.find({where: { id: In(ids)}, relations: ["category"]});
};
