import {getDataSource} from "../../../database/database";
import {Venue} from "../../../entities/venue";

export const venueResolver = async function (obj, {id}, context, info) {
    const repository = getDataSource().getRepository(Venue);
    return await repository.findOneOrFail({where: {id: id}});
};
