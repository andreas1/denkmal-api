import {User} from "../../../entities/user";
import {getDataSource} from "../../../database/database";

export const userResolver = async function (obj, {id}, context, info) {
    const userRespository = getDataSource().getRepository(User);
    return userRespository.findOneOrFail({where: {id: id}});
};