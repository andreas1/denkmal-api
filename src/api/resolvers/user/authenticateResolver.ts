import {User} from "../../../entities/user";
import {checkPassword} from "../../authentication/hashPassword";
import {createToken} from "../../authentication/token";
import {getDataSource} from "../../../database/database";

export const authenticateResolver = async function (obj, attrs, context, info) {
    const entityManager = getDataSource().manager;

    const user = await entityManager.findOne(User, {where: {email: attrs.email}});

    if (!user) {
        throw new Error("user not found");
    }

    if (checkPassword(attrs.password, user.password) !== true) {
        throw new Error("wrong password");
    }

    const token = await createToken(user);

    return {
        user: user,
        token: token
    };
};