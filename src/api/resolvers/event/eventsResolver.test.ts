import {createTestServer} from "../../../test/testServer";
import {gql} from 'graphql-tag';
import {AccessLevelEnum} from "../../../entities/user";
import {getDataSource} from "../../../database/database";

describe("eventsResolver", () => {
    let server;

    beforeEach(async () => {
        server = await createTestServer(AccessLevelEnum.Admin);

        let resr1 = await server.executeOperation({
            query: gql`
                mutation {addRegion(
                    name: "Zurich"
                    slug: "region1"
                    latitude: 10
                    longitude: 20
                    timeZone: "Europe/Zurich"
                    dayOffset: 5
                    email: "region1@asdf.ch"
                ){id}}
            `
        });
        let resr2 = await server.executeOperation({
            query: gql`
                mutation {addRegion(
                    name: "Hong Kong"
                    slug: "region2"
                    latitude: 30
                    longitude: 54
                    timeZone: "Asia/Hong_Kong"
                    dayOffset: 3
                    email: "region2@asdf.ch"
                ){id}}
            `
        });
        let regionId1 = resr1.data.addRegion.id;
        let regionId2 = resr2.data.addRegion.id;

        let resv1 = await server.executeOperation({
            query: gql`
                mutation {addVenue(
                    regionId: "${regionId1}"
                    name: "venue-1"
                    address: "venue address"
                    url: "https://venue.com"
                    email: "ven@ue.com"
                    longitude: 3.1
                    latitude: -3.2
                    isReviewPending: false
                ){id}}
            `
        });
        let venueId1 = resv1.data.addVenue.id;

        let resv2 = await server.executeOperation({
            query: gql`
                mutation {addVenue(
                    regionId: "${regionId1}"
                    name: "venue-2"
                    address: "venue address 2"
                    url: "https://venue2.com"
                    email: "ven@ue2.com"
                    isReviewPending: false
                ){id}}
            `
        });
        let venueId2 = resv2.data.addVenue.id;

        let resv3 = await server.executeOperation({
            query: gql`
                mutation {addVenue(
                    regionId: "${regionId2}"
                    name: "venue-3"
                    address: "venue address 3"
                    url: "https://venue3.com"
                    email: "ven@ue3.com"
                    longitude: 4.1
                    latitude: -5.2
                    isReviewPending: false
                ){id}}
            `
        });
        let venueId3 = resv3.data.addVenue.id;

        let resgc1 = await server.executeOperation({
            query: gql`
                mutation {addGenreCategory(
                    regionId: "${regionId1}"
                    color: "fdfdfd"
                    name: "gcat1"
                ){id}}
            `
        });
        let genreCategoryId1 = resgc1.data.addGenreCategory.id;

        let resgc2 = await server.executeOperation({
            query: gql`
                mutation {addGenreCategory(
                    regionId: "${regionId2}"
                    color: "fdfdfd"
                    name: "gcat2"
                ){id}}
            `
        });
        let genreCategoryId2 = resgc2.data.addGenreCategory.id;

        let resg1 = await server.executeOperation({
            query: gql`
                mutation {addGenre(
                    name: "punk"
                    genreCategoryId: "${genreCategoryId1}"
                ){id}}
            `
        });
        let genreId1 = resg1.data.addGenre.id;
        let resg2 = await server.executeOperation({
            query: gql`
                mutation {addGenre(
                    name: "rock"
                    genreCategoryId: "${genreCategoryId1}"
                ){id}}
            `
        });
        let genreId2 = resg2.data.addGenre.id;

        let resg3 = await server.executeOperation({
            query: gql`
                mutation {addGenre(
                    name: "another genre"
                    genreCategoryId: "${genreCategoryId2}"
                ){id}}
            `
        });
        let genreId3 = resg3.data.addGenre.id;


        await server.executeOperation({
            query: gql`
                mutation {addEvent(
                    description: "ev1"
                    venueId: "${venueId1}"
                    from: "2030-05-06T13:00"
                    until: "2030-05-06T14:20"
                    hasTime: true
                    links: [{label: "l", url: "https://url.com"}]
                    isPromoted: true
                    isHidden: false
                    genres: ["${genreId1}", "${genreId2}"]
                ){id}}
            `
        });
        await server.executeOperation({
            query: gql`
                mutation {addEvent(
                    description: "ev2"
                    venueId: "${venueId2}"
                    from: "2030-05-06T13:10"
                    until: "2030-05-06T16:50"
                    hasTime: true
                    links: [{label: "ld", url: "https://link.com"}]
                    isPromoted: false
                    isHidden: false
                    genres: ["${genreId2}"]
                ){id}}
            `
        });
        await server.executeOperation({
            query: gql`
                mutation {addEvent(
                    description: "ev3"
                    venueId: "${venueId1}"
                    from: "2030-05-07T12:10"
                    hasTime: true
                    links: [{label: "ld", url: "https://link.com"}]
                    isPromoted: false
                    isHidden: false
                    genres: ["${genreId1}"]
                ){id}}
            `
        });
        await server.executeOperation({
            query: gql`
                mutation {addEvent(
                    description: "ev3"
                    venueId: "${venueId3}"
                    from: "2030-05-06T12:10"
                    until: "2030-05-06T20:10"
                    hasTime: true
                    links: [{label: "ld", url: "https://link.com"}]
                    isPromoted: true
                    isHidden: false
                    genres: ["${genreId3}"]
                ){id}}
            `
        });
    });

    afterEach(async () => {
        await getDataSource().destroy();
    });

    test("query events", async () => {
        let query1 = await server.executeOperation({
            query: gql`
                query {
                    regions{
                        slug
                        name
                        email
                        events(eventDays:["2030-05-04", "2030-05-05", "2030-05-06"], withHidden: false){
                            description
                            from
                            until
                            venue{
                                name
                                address
                                latitude
                                longitude
                                url
                                email
                            }
                            isHidden
                            links{label, url}
                            genres{name category {name color}}
                            hasTime
                        }
                    }
                }
            `
        });

        expect(query1).toMatchSnapshot("event days");
    });
});