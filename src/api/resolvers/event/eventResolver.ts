import {ApiEvent} from "api/helpers/apiEvent";
import {EntityNotFoundError} from "typeorm/error/EntityNotFoundError";
import {Event} from "../../../entities/event";
import {getEventRepository} from "../../../repository/eventRepository";
import {getDataSource} from "../../../database/database";

export const eventResolver = async function(obj, { id }, context, info) {
    const repository = getEventRepository(getDataSource());
    const query = repository.createQueryBuilder("event")
        .where({id: id})
        .innerJoinAndSelect("event.activeVersion", "activeVersion")
        .leftJoinAndSelect("activeVersion.genres", "genres")
        .leftJoinAndSelect("genres.category", "category");
    const event = await query.getOne()

    if (!event) {
        throw new EntityNotFoundError(Event,id);
    }

    return ApiEvent.apiEventForEvent(event);
};