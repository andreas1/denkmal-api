import {ApiEvent} from "api/helpers/apiEvent";
import {Venue} from "entities/venue";
import {getEventRepository} from "../../../repository/eventRepository";
import {getDataSource} from "../../../database/database";

export const upcomingVenueEventsResolver = async function(obj, args, context, info) {
    let venue = obj as Venue;
    let region = await venue.region;

    let [startDay] = region.getEventDayRangeNow();

    const repository = getEventRepository(getDataSource());

    const query = repository.createQueryBuilder("event");
    query.andWhere("event.venueId = :id", {id: venue.id});
    query.andWhere("activeVersion.from >= :fromDate", {fromDate: startDay.toDate()});
    query.addOrderBy("activeVersion.from", "ASC");
    query.leftJoinAndSelect("event.activeVersion", "activeVersion");
    query.leftJoinAndSelect("activeVersion.genres", "genres");
    query.leftJoinAndSelect("genres.category", "category");

    const apiEvents = (await query.getMany()).map(async e => await ApiEvent.apiEventForEvent(e));
    return apiEvents;
};