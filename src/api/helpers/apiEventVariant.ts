import {Event} from "entities/event";
import {AbstractApiEvent} from "./abstractApiEvent";
import {EventVersion} from "entities/eventVersion";
import {ApiEvent} from "./apiEvent";

export class ApiEventVariant extends AbstractApiEvent {
    eventVersion: EventVersion;

    _event?: Event;
    async event() : Promise<ApiEvent> {
        if (!this._event) {
            this._event = await this.eventVersion.event;
        }
        return await ApiEvent.apiEventForEvent(this._event);
    }

    static async apiEventVariantForEventVersion(eventVersion: EventVersion, event: Event) : Promise<ApiEventVariant> {
        let timeZone = (await (await event.venue).region).timeZone;
        const eventVariant = new ApiEventVariant();
        eventVariant.id = eventVersion.id;
        eventVariant.createdAt = eventVersion.createdAt;
        eventVariant.from = eventVersion.from.tz(timeZone);
        eventVariant.until = eventVersion.until ? eventVersion.until.tz(timeZone) : null;
        eventVariant.description = eventVersion.description;
        eventVariant.isReviewPending = eventVersion.isReviewPending;
        eventVariant.eventVersion = eventVersion;
        eventVariant.links = eventVersion.links;
        eventVariant.genres = eventVersion.genres;
        eventVariant.tags = eventVersion.tags;
        eventVariant.hasTime = eventVersion.hasTime;
        eventVariant._event = event;

        return eventVariant;
    }
}