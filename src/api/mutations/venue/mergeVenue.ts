import {getVenueRepository} from "../../../repository/venueRepository";
import {getDataSource} from "../../../database/database";

export const mergeVenueMutation = async function (_, {fromId, toId}) {
    const venueRepository = getVenueRepository(getDataSource());

    await venueRepository.mergeVenue(fromId, toId);

    return await venueRepository.findOne({where: {id: toId}, loadEagerRelations: true});
};