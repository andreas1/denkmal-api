import {Genre} from "../../../entities/genre";
import {GenreCategory} from "../../../entities/genreCategory";
import {getDataSource} from "../../../database/database";

export const changeGenreMutation = async function (_, attrs) {
    const entityManager = getDataSource().manager;
    const genre = await entityManager.findOneOrFail(Genre, {where: {id: attrs.id}});

    const entity = {
        id: genre.id,
        ...attrs,
        isReviewPending: false
    };
    if (attrs.genreCategoryId) {
        entity.category = await entityManager.findOneOrFail(GenreCategory, {where: {id: attrs.genreCategoryId}})
    }

    await entityManager.save(Genre, entity);

    return await entityManager.findOneOrFail(Genre, {where: {id: attrs.id}, relations: ["category"]});
};
