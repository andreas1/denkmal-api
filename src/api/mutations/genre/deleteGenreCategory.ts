import {GenreCategory} from "../../../entities/genreCategory";
import {getDataSource} from "../../../database/database";

export const deleteGenreCategoryMutation = async function (_, attrs) {
    const entityManager = getDataSource().manager;
    const genreCategoryId = attrs.id;
    await entityManager.delete(GenreCategory, {id: genreCategoryId});
    return true;
};