import {Genre} from "../../../entities/genre";
import {GenreCategory} from "../../../entities/genreCategory";
import {getDataSource} from "../../../database/database";


export const addGenreMutation = async function (_, attrs) {
    const dataSource = getDataSource();

    const genre = {
        ...attrs,
        isReviewPending: false,
        category: await dataSource.manager.findOneOrFail(GenreCategory, {where: {id: attrs.genreCategoryId}})
    }

    return dataSource.manager.save(Object.assign(new Genre(), genre));
};
