import {ApiEvent} from "api/helpers/apiEvent";
import {getEventRepository} from "../../../repository/eventRepository";
import {getDataSource} from "../../../database/database";

export const promoteEventMutation = async function (_, attrs) {
    const eventRepository = getEventRepository(getDataSource());
    const event = await eventRepository.findOneOrFail({where: { id: attrs.id}});

    event.isPromoted = attrs.promote == true;

    await eventRepository.save(event);

    return ApiEvent.apiEventForEvent(event);
};