import {gql} from 'graphql-tag'
import {Genre} from "../../../entities/genre";
import {AccessLevelEnum} from "../../../entities/user";
import {createTestServer} from "../../../test/testServer";
import moment = require("moment-timezone");
import {getDataSource, truncateAllTables} from "../../../database/database";
import {getEventRepository} from "../../../repository/eventRepository";
import {getVenueRepository} from "../../../repository/venueRepository";

describe("addScraperEvent", () => {
    let server;
    let regionId;

    beforeAll(async () => {
        server = await createTestServer(AccessLevelEnum.Admin);
    });

    beforeEach(async () => {
        await truncateAllTables();
        let res1 = await server.executeOperation({
            query: gql`
                mutation {addRegion(
                    name: "MyRegion"
                    slug: "addScraperEvent-region-1"
                    latitude: 40
                    longitude: 50
                    timeZone: "Europe/Zurich"
                    dayOffset: 0
                    email: "mail@asdf.ch"
                ){id}}
            `
        });
        regionId = res1.data.addRegion.id;
    });

    async function queryEvents(eventDay: string, includeDates: boolean) {
        return await server.executeOperation({
            query: gql`
                query {
                    regions{
                        venues{name}
                        events(eventDays:["${eventDay}"], withHidden: true){
                            description
                            ${includeDates ? "from" : ""}
                            ${includeDates ? "until" : ""}
                            sourceIdentifier
                            venue{name}
                            isReviewPending
                            isHidden
                            links{label, url}
                            genres {name}
                            tags
                            variants{isReviewPending, description}
                            hasTime
                        }
                    }
                }
            `
        });
    }


    test("Add new previously undiscovered event with unknown venue, then add more versions", async () => {
        let fromDate = moment.tz("Europe/Zurich").add(2, 'days');
        let untilDate = fromDate.clone().add(3, 'hours');
        let eventDayString = fromDate.format('YYYY-MM-DD');

        let result1 = await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [{
                        regionId: "${regionId}"
                        sourceIdentifier: "source-1"
                        sourceUrl: "https://myurl"
                        venueName: "venue-1"
                        description: "My Description"
                        from: "${fromDate.toISOString()}"
                        until: "${untilDate.toISOString()}"
                        links: [{label: "link", url: "https://bla.ch"}]
                        genres: ["one", "two", "three"]
                    }]
                )}
            `
        });
        expect(result1.errors).toBeUndefined();

        {
            const eventsQuery = await queryEvents(eventDayString, false);

            const createdEvent = eventsQuery.data.regions[0].events[0][0];

            expect(createdEvent.isReviewPending).toBeTruthy();
            expect(createdEvent.isHidden).toBeTruthy();

            expect(eventsQuery).toMatchSnapshot("create unknown event");
        }

        // get the event id (can't be included in the snapshots)
        const eventIdQuery = await server.executeOperation({
            query: gql`
                query {
                    regions{
                        venues{name}
                        events(eventDays:["${eventDayString}"], withHidden: true){
                            id
                        }
                    }
                }
            `
        });
        const eventId = eventIdQuery.data.regions[0].events[0][0].id;


        // add the event from another source
        let result2 = await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "source-2"
                            sourceUrl: "https://myurl"
                            venueName: "venue-1"
                            description: "My Description source 2"
                            from: "${fromDate.toISOString()}"
                            until: "${untilDate.toISOString()}"
                            links: [{label: "link", url: "https://bla.ch"}]
                            genres: ["one", "two", "three"]
                        }
                    ]
                )}
            `
        });
        expect(result2.errors).toBeUndefined();

        {
            const eventsQuery = await queryEvents(eventDayString, false);

            const events = eventsQuery.data.regions[0].events[0];
            expect(events.length).toEqual(1);
            const createdEvent = events[0];
            expect(createdEvent.variants.length).toEqual(1);

            expect(eventsQuery).toMatchSnapshot("create second event version");
        }


        // add the event from the first source again
        let result3 = await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [{
                        regionId: "${regionId}"
                        sourceIdentifier: "source-1"
                        sourceUrl: "https://myurl"
                        venueName: "venue-1"
                        description: "My Description extended"
                        from: "${fromDate.toISOString()}"
                        until: "${untilDate.toISOString()}"
                        links: [{label: "link", url: "https://bla.ch"}]
                        genres: ["one", "two", "three"]
                    }]
                )}
            `
        });
        expect(result3.errors).toBeUndefined();

        {
            const eventsQuery = await queryEvents(eventDayString, false);

            const events = eventsQuery.data.regions[0].events[0];
            expect(events.length).toEqual(1);
            const createdEvent = events[0];
            expect(createdEvent.isReviewPending).toBeTruthy();
            expect(createdEvent.description).toEqual("My Description extended");
            expect(createdEvent.sourceIdentifier).toEqual("source-1");
            expect(createdEvent.isHidden).toBeTruthy();
            expect(createdEvent.variants.length).toEqual(1);

            expect(eventsQuery).toMatchSnapshot("update unreviewed event");
        }

        // review the event
        let result4 = await server.executeOperation({
            query: gql`
                mutation {reviewEvent(
                    id: "${eventId}"
                    description: "Some description",
                    tags: ["a tag", "another tag"]
                ){id}}
            `
        });
        expect(result4.errors).toBeUndefined();

        {
            let eventsQuery = await queryEvents(eventDayString, false);

            const events = eventsQuery.data.regions[0].events[0];
            expect(events.length).toEqual(1);
            const createdEvent = events[0];
            expect(createdEvent.description).toEqual("Some description");
            expect(createdEvent.isReviewPending).toBeFalsy();
            expect(createdEvent.isHidden).toBeTruthy();
            expect(createdEvent.sourceIdentifier).toBeNull();
            expect(createdEvent.variants.length).toEqual(2);

            expect(eventsQuery).toMatchSnapshot("reviewed event");
        }

        let result5 = await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [{
                        regionId: "${regionId}"
                        sourceIdentifier: "source-1"
                        sourceUrl: "https://myurl"
                        venueName: "venue-1"
                        description: "New changes after admin review"
                        from: "${fromDate.toISOString()}"
                        until: "${untilDate.toISOString()}"
                        links: [{label: "link", url: "https://bla.ch"}]
                        genres: ["one", "two", "three"]
                    }]
                )}
            `
        });
        expect(result5.errors).toBeUndefined();
        {
            const eventsQuery = await queryEvents(eventDayString, false);
            expect(eventsQuery).toMatchSnapshot("scraper updated reviewed event");
        }
    });


    test("event duplication prevention", async () => {
        let fromDate = moment.tz("Europe/Zurich").add(2, 'days').startOf("day").subtract(2, 'hours');

        for (let i = 0; i < 4; i++) {
            let eventDayString = fromDate.format('YYYY-MM-DD');

            let result1 = await server.executeOperation({
                query: gql`
                    mutation {addScraperEvents(
                        events: [{
                            regionId: "${regionId}"
                            sourceIdentifier: "source-1"
                            sourceUrl: "https://myurl"
                            venueName: "venue-1"
                            description: "My Description"
                            from: "${fromDate.toISOString()}"
                        }]
                    )}
                `
            });
            expect(result1.errors).toBeUndefined();

            let result2 = await server.executeOperation({
                query: gql`
                    mutation {addScraperEvents(
                        events: [{
                            regionId: "${regionId}"
                            sourceIdentifier: "anothersource"
                            sourceUrl: "https://bla"
                            venueName: "venue-1"
                            description: "My Description"
                            from: "${fromDate.toISOString()}"
                        }]
                    )}
                `
            });
            expect(result2.errors).toBeUndefined();

            const eventsQuery = await queryEvents(eventDayString, false);
            expect(eventsQuery.data.regions[0].events[0].length).toEqual(1);


            const eventRepository = getEventRepository(getDataSource());
            await eventRepository.remove((await eventRepository.find({}))[0]);

            fromDate = fromDate.clone().add(1, 'hour');
        }
    });

    test("passed venue and genres result in venue and genres pending review", async () => {
        let fromDate = moment.tz("Europe/Zurich").add(2, 'days').startOf("day").subtract(5, 'hours');

        let venueName = "venue-pending-review";
        let tag1 = "tag-pending-review1";
        let tag2 = "tag-pending-review2";

        let result = await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [{
                        regionId: "${regionId}"
                        sourceIdentifier: "anothersource"
                        sourceUrl: "https://bla"
                        venueName: "${venueName}"
                        description: "My Description"
                        from: "${fromDate.toISOString()}"
                        genres: ["${tag1}", "${tag2}"]
                    }]
                )}
            `
        });
        expect(result.errors).toBeUndefined();

        const venueRepository = getVenueRepository(getDataSource());
        const venue = await venueRepository.findOne({where: {name: venueName}});
        expect(venue).toBeDefined();
        expect(venue.isReviewPending).toBeTruthy();

        const tagRepository = getDataSource().getRepository(Genre);
        const tag1Find = await tagRepository.findOne({where: {name: tag1}});
        expect(tag1Find).toBeDefined();
        expect(tag1Find.isReviewPending).toBeTruthy();
        const tag2Find = await tagRepository.findOne({where: {name: tag2}});
        expect(tag2Find).toBeDefined();
        expect(tag2Find.isReviewPending).toBeTruthy();
    });

    test("Create events in sequence, with same genre", async () => {
        let fromDate = moment.tz("Europe/Zurich").add(2, 'days').set('hour', 22);

        let result = await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "unit-test"
                            sourceUrl: "https://bla"
                            venueName: "My venue 1"
                            description: "My Description 1"
                            from: "${fromDate.toISOString()}"
                            genres: ["my-genre-1", "my-genre-2"]
                        },
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "unit-test"
                            sourceUrl: "https://bla"
                            venueName: "My venue 2"
                            description: "My Description 2"
                            from: "${fromDate.toISOString()}"
                            genres: ["my-genre-1", "my-genre-3"]
                        }
                    ]
                )}
            `
        });
        expect(result.errors).toBeUndefined();

        const eventRepo = getEventRepository(getDataSource());
        expect(await eventRepo.count()).toEqual(2);

        const tagRepository = getDataSource().getRepository(Genre);
        expect(await tagRepository.count()).toEqual(3);
    });

    test("source priorities", async () => {
        let fromDate = moment.tz("Europe/Zurich").add(2, 'days').set('hour', 22);
        let eventDayString = fromDate.format('YYYY-MM-DD');

        await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "source1"
                            sourceUrl: "https://source1"
                            sourcePriority: 2
                            venueName: "My venue 1"
                            description: "One"
                            from: "${fromDate.toISOString()}"
                        }
                    ]
                )}
            `
        });

        await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "source2"
                            sourceUrl: "https://source2"
                            sourcePriority: 4
                            venueName: "My venue 1"
                            description: "Two"
                            from: "${fromDate.toISOString()}"
                        }
                    ]
                )}
            `
        });

        await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "source3"
                            sourceUrl: "https://source3"
                            sourcePriority: 1
                            venueName: "My venue 1"
                            description: "Three"
                            from: "${fromDate.toISOString()}"
                        }
                    ]
                )}
            `
        });

        const eventsQuery = await queryEvents(eventDayString, true);
        const event = eventsQuery.data.regions[0].events[0][0];
        expect(event).toBeDefined();
        expect(event.description).toEqual("Two");
    });


    test("multiple events per venue and eventDay test", async () => {
        let firstFromDate = moment.tz("Europe/Zurich").add(2, 'days').set('hour', 14);
        let secondFromDate = moment.tz("Europe/Zurich").add(2, 'days').set('hour', 16);

        let eventDayString = firstFromDate.format('YYYY-MM-DD');

        await server.executeOperation({
            query: gql`
                mutation {addScraperEvents(
                    events: [
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "doubleSource"
                            sourceUrl: "https://doubleSource"
                            sourcePriority: 2
                            venueName: "DoubleSource venue"
                            description: "First party"
                            from: "${firstFromDate.toISOString()}"
                        },
                        {
                            regionId: "${regionId}"
                            sourceIdentifier: "doubleSource"
                            sourceUrl: "https://doubleSource"
                            sourcePriority: 2
                            venueName: "DoubleSource venue"
                            description: "Second party"
                            from: "${secondFromDate.toISOString()}"
                        }
                    ]
                )}
            `
        });

        const eventsQuery = await queryEvents(eventDayString, true);
        const events = eventsQuery.data.regions[0].events[0];
        expect(events).toHaveLength(2);
    });
});
