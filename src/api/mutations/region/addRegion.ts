import {Region} from 'entities/region';
import {getDataSource} from "../../../database/database";

export const addRegionMutation = async function (_, attrs) {
    const region = {
        ...attrs,
    }

    return await getDataSource().manager.save(Object.assign(new Region(), region));
};
