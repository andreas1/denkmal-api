import {Region} from "../../../entities/region";
import {getDataSource} from "../../../database/database";

export const deleteRegionMutation = async function (_, attrs) {
    const entityManager = getDataSource().manager;
    const regionId = attrs.id;
    await entityManager.findOneOrFail(Region, {where: {id: regionId}});

    return entityManager.delete(Region, {id: regionId});
};