import {User} from "../../entities/user";
import {createToken, getUserWithToken} from "./token";
import {getDataSource, initializeDataSource} from "../../database/database";

describe("test auth tokens", () => {
    let user: User = null

    beforeEach(async () => {
        const dataSource = await initializeDataSource(true);

        user = new User();
        user.name = "test"
        user.email = "test@test.com"
        user.password = "password"
        user = await dataSource.manager.save(user);
    })

    afterEach(async () => {
        await getDataSource().destroy();
    });

    test('create and verify', async() => {
        const token = await createToken(user)
        expect(token).toBeTruthy()

        const foundUser = await getUserWithToken(token)
        expect(foundUser).toBeDefined()
        expect(foundUser.id).toEqual(user.id)
    })
})