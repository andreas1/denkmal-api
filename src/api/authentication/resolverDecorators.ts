import {AccessLevelEnum} from "../../entities/user";
import {GraphQLError} from "graphql";

export const restrictAccess = function(accessLevel: AccessLevelEnum | Array<AccessLevelEnum>, resolver) {
    let accessLevels = [];
    if (Array.isArray(accessLevel)) {
        accessLevels = accessLevel;
    } else {
        accessLevels = [accessLevel];
    }

    return function(parent, args, contextValue, info) {

        if (!contextValue.user || !accessLevels.includes(contextValue.user.accessLevel)) {
            throw new GraphQLError('no access', {
                extensions: {
                    code: 'FORBIDDEN',
                },
            });
        }

        return resolver(parent, args, contextValue, info);
    };
};