import {Logger as LoggerInterface} from "typeorm";
import {Logger} from "./logger";

import {QueryRunner} from "typeorm/query-runner/QueryRunner";

export class TypeormLogger implements LoggerInterface {
    logger : Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    /**
     * Logs query and parameters used in it.
     */
    logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner): any {
        this.logger.debug(query, )
    }
    /**
     * Logs query that is failed.
     */
    logQueryError(error: string, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
        this.logger.error(`${error} in query: ${query}`);
    }
    /**
     * Logs query that is slow.
     */
    logQuerySlow(time: number, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
        this.logger.warn(`query "${query}" seems slow (${time})`);
    }
    /**
     * Logs events from the schema build process.
     */
    logSchemaBuild(message: string, queryRunner?: QueryRunner): any {
        this.logger.info(message);
    }
    /**
     * Logs events from the migrations run process.
     */
    logMigration(message: string, queryRunner?: QueryRunner): any {
        this.logger.info(message);
    }
    /**
     * Perform logging using given logger, or by default to the console.
     * Log has its own level and message.
     */
    log(level: "log" | "info" | "warn", message: any, queryRunner?: QueryRunner): any {
        switch(level) {
            case "log":
                this.logger.debug(message);
                break;
            case "info":
                this.logger.info(message);
                break;
            case "warn":
                this.logger.warn(message);
                break;
        }
    }
}