import {
    Column,
    CreateDateColumn,
    Entity,
    Index, ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn, RelationId, Unique,
    UpdateDateColumn
} from "typeorm";
import {GenreCategory} from "./genreCategory";
import moment = require("moment-timezone");
import {sharedDateTransformer} from "./transformers/dateTransformer";
import {EventVersion} from "./eventVersion";
import {sharedLowercaseTransformer} from "./transformers/lowercaseTransformer";

@Entity('genres')
@Unique(["category", "name"])
export class Genre {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('boolean', {default: true})
    @Index()
    isReviewPending: boolean;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: moment.Moment;

    @UpdateDateColumn({transformer: sharedDateTransformer})
    updatedAt: moment.Moment;

    @Column({transformer: sharedLowercaseTransformer})
    @Index()
    name: string;

    @ManyToOne(type => GenreCategory, genreCategory => genreCategory.genres, {
        onDelete: "CASCADE"
    })
    @Index()
    category: GenreCategory;

    @RelationId("category")
    categoryId: string;

    @ManyToMany(type => EventVersion, eventVersion => eventVersion.genres)
    eventVersions: EventVersion[];
}