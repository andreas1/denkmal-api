import {
    BeforeInsert,
    Column,
    CreateDateColumn,
    Entity,
    Index,
    ManyToOne,
    PrimaryGeneratedColumn,
    RelationId,
    Unique
} from "typeorm";
import {Venue} from "./venue";
import {sharedDateTransformer} from "./transformers/dateTransformer";
import {Moment} from 'moment-timezone';

@Entity('venueAliases')
@Unique(["regionId", "name"])
export class VenueAlias {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: Moment;

    @CreateDateColumn({transformer: sharedDateTransformer})
    updatedAt: Moment;

    @Column('text')
    name: string;

    @ManyToOne(type => Venue, venue => venue.aliases, {
            nullable: false,
            onDelete: "CASCADE"
        })
    @Index()
    venue: Venue;

    @RelationId((venueAlias: VenueAlias) => venueAlias.venue)
    venueId: string;

    @Column('uuid')
    regionId: string;

    @BeforeInsert()
    async setRegionId() {
        if (!this.regionId) {
            this.regionId = this.venue.regionId;
        }
    }
}